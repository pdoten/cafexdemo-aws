/**
 * Support starting a co-browse session by sharing generated short code.
 *
 * @type {{startSupport: Window.ShortCodeAssist.startSupport}}
 */
window.ShortCodeAssist = {
  startSupport: function(callback, configuration) {
      // get a cid/session token via the short code servlet
      var shortCodeRequest = new XMLHttpRequest();
      shortCodeRequest.onreadystatechange = function() {
          if (shortCodeRequest.readyState == 4) {
              if (shortCodeRequest.status == 200) {
                  var response = JSON.parse(shortCodeRequest.responseText);
                  if(typeof (configuration) === 'string' || typeof (configuration) === 'undefined') {
                      configuration = {};
                  }
                  
                  configuration.sessionToken = response.token;
                  configuration.correlationId = response.cid;
                  delete configuration.destination;
                  
                  if (!configuration.scaleFactor) {
                      configuration.scaleFactor = response.scaleFactor;
                  }
                  
                  configuration.allowedIframeOrigins = false; // important: disable iframe messaging if not required for security
                  AssistSDK.startSupport(configuration);
                  callback(response.cid);
              } else {
                 // TODO Report failure to start
              }
          }
      };
      shortCodeRequest.open("POST", "https://demo.jp.cafex.com/assistserver/shortcode/", true);
      shortCodeRequest.send();
  }
};
