<?php

header('Access-Control-Allow-Origin: *');

    // configure the curl options
    $ch = curl_init("https://sales.cloud1.cafex.com:8443/assistserver/shortcode/create");
    curl_setopt($ch,CURLOPT_PUT, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);	
    curl_setopt($ch, CURLOPT_HTTPHEADER,'Content-Type: application/json','Content-Length: ' . strlen($json));

    // execute HTTP POST & close the connection
    $response = curl_exec($ch);
    curl_close($ch);

    // decode the JSON and pick out the session token
    $decodedJson = json_decode($response);
    $code = $decodedJson->{'shortCode'};

    // echo the ID we've retrieved
    echo $response;


?>
