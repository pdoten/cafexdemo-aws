;(function() {
    
    var SESSION_DATA_NAME = "assist-session-config";
    var storage = localStorage || false;
        
    window.Config = function(configuration) {
        this.hasVideo = function() {
            if (this.getVideoMode() == "none") {
                return false;
            }
            return true;
        };
        
        this.hasLocalVideo = function() {
        	return (this.getVideoMode() == "full");
        }
        
        this.hasAudio = function() {
            if (!this.hasDestination()) {
                return false;
            }
            return true;
        };
        
        this.hasDestination = function() {
            if ("destination" in configuration) {
                return true;
            }
            return false;
        };

        this.getSessionToken = function() {
            return configuration.sessionToken;
        };

        this.setSessionToken = function(sessionToken) {
            if (sessionToken && !configuration.sessionToken) {
                configuration.sessionToken = sessionToken;
            }
        };

        this.getDestination = function() {
            return configuration.destination;
        };
        
        this.getUrl = function() {
            return configuration.url;
        };
        
        this.getCorrelationId = function() {
            return configuration.correlationId;
        };
        
        this.setCorrelationId = function(correlationId) {
            configuration.correlationId = correlationId;
        };
        
        this.getVideoMode = function() {
            if (this.hasDestination() == false) {
                return "none";
            } else if ("videoMode" in configuration && typeof configuration["videoMode"] != 'undefined') {
                return configuration.videoMode;
            } else {
                return "full";
            }
        };
        
        this.getStunServers = function() {
            return configuration.stunservers;
        };
        
        this.getTargetServer = function() {
            return getEncodedServerAddr(this.getUrl());
        };
        
        this.getOriginServer = function() {
            return getEncodedServerAddr();
        };
        
        this.getConnectionEstablished = function() {
        	return configuration.connectionEstablished;
        };
        
        this.getDebugMode = function() {
        	return configuration.debugMode;
        }
        
        this.unset = function() {
            if (storage) {
                storage.removeItem(SESSION_DATA_NAME);
            }
        }
        
        ;(function init() {
     
        })();
    }
    
    window.AssistConfig = (function() {
        if (configuration in window && typeof configuration != 'undefined') {
            return new window.Config(configuration);
        } else {    	
            var configuration = storage.getItem(SESSION_DATA_NAME);
            if (configuration !== false || configuration !== "false") {
                return new window.Config(JSON.parse(configuration));
            } else {
                return false;
            }
        }
    })();

    function getEncodedServerAddr(url) {
        var protocol = window.location.protocol;
        var port = window.location.port;
        var hostname = window.location.hostname;

        var urlToEncode;
        if (url == null) {
            if (port === "") {
                urlToEncode = protocol + "//" + hostname;
            } else {
                urlToEncode = protocol + "//" + hostname + ":" + port;
            }
        } else {
            urlToEncode = url;
        }

        return encodeURIComponent(AssistUtils.Base64.encode(urlToEncode));
    }
})();
