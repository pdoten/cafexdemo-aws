var QueryString = function () {
    // This function is anonymous, is executed immediately and
    // the return value is assigned to QueryString!
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
            // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = pair[1];
            // If second entry with this name
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [ query_string[pair[0]], pair[1] ];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
        } else {
            query_string[pair[0]].push(pair[1]);
        }
    }
    return query_string;
} ();

function assistConfig() {
		
	function selectMode() {
	
		var settings = {};
		
		if (QueryString.remoteServer) {
			settings.url = QueryString.remoteServer;
		}
		
		if (QueryString.username) {
			settings.username = QueryString.username;
		}

        if (QueryString.sessionToken) {
            settings.sessionToken = QueryString.sessionToken;
        }
        
        if (QueryString.videoMode) {
        	settings.videoMode = QueryString.videoMode;
        }

        if (QueryString.cobrowseOnly == 'true') {
            console.log("Cobrowse-only mode enabled");
            settings.cobrowseOnly = true;
        } else {
            if (QueryString.agent) {
    			console.log("Assist Calling agent: " + QueryString.agent);
		    	settings.destination = QueryString.agent;
        	} else {
	    		console.log("Assist Calling agent: agent1");
		    	settings.destination = 'agent1';
    		}
        }

        if (QueryString.cid) {
			console.log("Assist Calling correlationId: " + QueryString.cid);
			settings.correlationId = QueryString.cid;
		} 		
		if (QueryString.lang) {
		    console.log("Assist UI elements using locale: " + QueryString.lang);
		    settings.locale = QueryString.lang;
		}
		
		if (QueryString.debug == 'true') {
			console.log("Debug mode enabled");
			settings.debugMode = true;
		}
		return settings;
	}
	
	return selectMode();
}
		
